# tgbot, a docker image for golem-telegram-bot
## Build
```
docker build --tag hub.docker.golem.linux.it/golem-telegram-bot:latest .
```

## Permissions
```
drwxrwxr-x  root    www-data    /data
```

## Bot development
* mount golem-telegram-bot repository as volume under /var/www/html
* ```mv $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini```
(this enables error loggin on docker stdout/stderr logs)

