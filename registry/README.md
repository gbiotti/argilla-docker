# registry

## add user
```htpasswd -Bn username >> htpasswd```

## edit user
```vim htpasswd```

## generate certificate
```openssl req -x509 -newkey rsa:4096 -nodes -keyout registry.key -out registry.crt -days 3650```

## list available images
```curl -X GET https://dockerhub.golem.linux.it/v2/_catalog --user golem```
